
var xml$dt = {};
xml$dt.father = {};

xml$dt.process = function(str, conf) {
    var parser = new DOMParser();
    var xml = parser.parseFromString(str, "text/xml");

    if (!('#document' in conf)) {
	    conf['#document'] = (q,c,v) => { return c; };
    }

    if (!('#reduce' in conf)) {
        conf['#reduce'] = { };
    }

    if (!('#default' in conf['#reduce'])) {
        conf['#reduce']['#default'] = (children) => {
            return children.length > 1 ? children.join("") : children[0];
        };
    }

    if ('#map' in conf) {
        var keys = Object.keys(conf['#map']);
        for (var i = 0; i < keys.length; i++) {
            /* Yep, this is a closure! */
            ( () => {
		        var outTag = conf['#map'][keys[i]];
		        conf[keys[i]] = (q,c,v) => { return xml$dt.tag( outTag, c,v); };
            })();
        }
    }
    
    return xml$dt.__dt(xml, conf);
};

// The recursive dt function
xml$dt.__dt = function (element, conf) {
    var child;
    var extraAttributes = {};

    var nodeName = element.nodeName;

    if (element.childNodes.length > 0) {
        childs = xml$dt.__map(element.childNodes, (x) => {
	        xml$dt.father = {};
            var r = xml$dt.__dt(x, conf);
	        extraAttributes = xml$dt.__extend(extraAttributes, xml$dt.father);
	        return r;
        });

        if (nodeName in conf['#reduce']) {
            child = conf['#reduce'][nodeName](childs); 
        } else if ('#default' in conf['#reduce']) {
            child = conf['#reduce']['#default'](childs);
        }
    }
    else {
	    xml$dt.father = {};
	    child = element.data;
	    extraAttributes = xml$dt.father;
    }
    
    var attr = extraAttributes;
    var attributes = element.attributes; // nodeName / nodeValue
    for (var i = 0; attributes && i < attributes.length; i++) {
	    attr[attributes[i].nodeName] = attributes[i].nodeValue;
    }
    
    var result = child;
    if (element.nodeName in conf) {
	    result = conf[nodeName](nodeName, child, attr);
    }
    else if ("#default" in conf && nodeName != "#text") {
	    result = conf["#default"](nodeName, child, attr);	
    }
    
    return result;
};




// auxiliary method to create a tag
xml$dt.tag = function(name, content, attr) {
    attr = attr || {};
    
    var r = "<" + name;
    
    var keys = Object.keys(attr);
    keys.sort();
    for (var i = 0; i < keys.length; i++) {
	    r += " " + keys[i] + "=\"" + attr[keys[i]] + "\"";
    }
    
    if (content) {
    	r += ">" + content + "</" + name + ">";
    }
    else {
	    r += "/>";
    }
    return r;
};




// Just a framework agnostic map function
xml$dt.__map = function (list, func) {
    var r = [];
    for (var i = 0; i < list.length; i++) {
	    r.push(func(list[i]));
    }
    return r;
};


// framework agnostic dictionary extender
xml$dt.__extend = function(destination, source) {
    for (var property in source) {
        if (source.hasOwnProperty(property)) {
            destination[property] = source[property];
        }
    }
    return destination;
};
