
var tests = [
    [function(){ return xml$dt.tag("foo","bar")}, "<foo>bar</foo>", "tag"],
    [function(){ return xml$dt.tag("foo")}, "<foo/>", "tag"],
    [function(){ return xml$dt.tag("foo","bar",{"ugh": "zbr"})}, "<foo ugh=\"zbr\">bar</foo>", "tag"],
    [function(){ return xml$dt.tag("foo","bar",{"c":"d","a": "b"})}, "<foo a=\"b\" c=\"d\">bar</foo>", "tag"],
    [function(){ return xml$dt.tag("foo","",{"c":"d","a": "b"})}, "<foo a=\"b\" c=\"d\"/>", "tag"],
    [function(){ return xml$dt.tag("foo",null,{"c":"d","a": "b"})}, "<foo a=\"b\" c=\"d\"/>", "tag"],


    [function(){ return xml$dt.process("<foo/>", {foo: function() {return ""; }})}, "", "dt"],
    [function(){ return xml$dt.process("<foo/>", {foo: function(q, c, v) { return xml$dt.tag(q, c, v); }});}, "<foo/>", "dt"],

    [function(){ return xml$dt.process("<foo><bar/></foo>",
			   {foo: function(q, c, v) { return q+c; },
			    bar: function(q, c, v) { return q; }});}, "foobar", "dt"],
    
    [function(){ return xml$dt.process("<foo bar='zbr'/>",
			   {foo: function(q, c, v) { return v.bar; }});}, "zbr", "dt"],

    [function(){ return xml$dt.process("<foo>bar</foo>",
			   {foo: function(q, c, v) { return xml$dt.tag(q,c,v); },
			    '#text': function() { return "ugh"; }});}, "<foo>ugh</foo>", "dt"],

    [function(){ return xml$dt.process("<foo>bar</foo>",
			   {'#default': function(q, c, v) { return xml$dt.tag(q,c,v); },
			    '#text': function() { return "ugh"; }});}, "<foo>ugh</foo>", "dt"],
    [
	function() { return xml$dt.process("<foo>bar</foo>", { '#map': { 'foo': 'b' }}); }, "<b>bar</b>", "#map"
    ],
    [
	function() { return xml$dt.process("<a><b>c</b><d>e</d><f>g</f></a>",
			       { '#map': { 'a':'b','b':'c','d':'e','f':'g' }}); },
	"<b><c>c</c><e>e</e><g>g</g></b>", "#map"
    ],

    [
	function() {
	    return xml$dt.process("<foo><bar>x</bar></foo>", {
		'bar' : function(q,c,v) { xml$dt.father.tag = c; return ""; },
		'foo' : function(q,c,v) { return xml$dt.tag(v.tag, "ola"); }, 
	    });
	},
	"<x>ola</x>", "father"
    ],

    [
        () => {
            return xml$dt.process("<foo><x>y</x><x>z</x></foo>", {
                '#reduce' : {
                    'foo' : (child) => { return child; }
                },
                'x'       : (q,c,v) => { return c; },
                'foo'     : (q,c,v) => { return c.join(","); },
            });
        },
        "y,z", "#reduce"
    ],
];

function protect(s) {
    return s.replace(/>/g,"&gt;").replace(/</g,"&lt;");
}

function IS(left, right, desc) {
    desc = desc ?  " - " + desc : "";
    var obtained = left();
    var ok = obtained == right;
    var ans = (ok ? "OK" : "NOK" ) + desc;
    if (!ok) {
	ans += "\n  Obtained: "+ protect(obtained);
	ans += "\n  Expected: "+ protect(right);
    }
    return ans;
}

document.addEventListener("DOMContentLoaded", function(event) { 
    var table = document.getElementById("table");

    for (var i = 0; i < tests.length; i++) {
 	var row = table.insertRow(0);
 	var cell = row.insertCell(0);
 	cell.innerHTML = "<pre>" + IS(tests[i][0], tests[i][1], tests[i][2]) + "</pre>";
    };
});
